# Znalostní systémy

Dataset je ze zdroje `https://www.kaggle.com/mohansacharya/graduate-admissions`.

Program vyžaduje vytvoření prostředí dle souboru `environment.yml`.

Rule based systém a jeho pravidla jsou ke shlédnutí v souboru `app/rule_based.py`, oba lineární SVM a regresní strom jsou implementovány v souboru `app/ml_system.py`.

Po spuštění `app/__main__.py` dostanete výsledky v následujícím formátu pro každou testovací instanci.

| |
| --- |
| actual: 0.76 |
| ML model: 0.76 |
| Tree model: 0.73 |
| RB model: 0.75 |

* **Actual** je pravděpodonost přijetí udávaná v datasetu,
* **ML model** je hodnota vypočtená _lineárním SVM_,
* **Tree model** je hodnota vypočtená regresním stromem,
* **RB model** udává hodnotu z _rule-based_ systému.
