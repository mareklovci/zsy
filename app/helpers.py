#!/usr/bin/env python
# # -*- coding: utf-8 -*-

"""helpers.py: helper classes and functions for main application"""

from enum import IntEnum


class CGPA(IntEnum):
    Outstanding = 10,
    Excellent = 9,
    VeryGood = 8,
    Good = 7,
    Average = 6,
    BelowAverage = 5,
    Fail = 4


class GeneralRating(IntEnum):
    Excellent = 5,
    VeryGood = 4,
    Good = 3,
    Sufficient = 2,
    Satisfactory = 1


def get_cgpa_score(argument: int) -> CGPA:
    cgpa = round(argument)
    if cgpa > 10:
        raise Exception("Cumulative Grade Point Average score not defined!")
    elif cgpa < 4:
        return CGPA.Fail
    else:
        return CGPA(cgpa)


def cgpa_to_general(score: CGPA) -> GeneralRating:
    if score == CGPA.Outstanding:
        return GeneralRating.Excellent
    elif score in (CGPA.Excellent, CGPA.VeryGood):
        return GeneralRating.VeryGood
    elif score in (CGPA.Good, CGPA.Average):
        return GeneralRating.Good
    elif score == CGPA.BelowAverage:
        return GeneralRating.Sufficient
    elif score == CGPA.Fail:
        return GeneralRating.Satisfactory


def get_toefl_score(score: int) -> GeneralRating:
    if score >= 100:
        return GeneralRating.Excellent
    if 100 > score >= 80:
        return GeneralRating.VeryGood
    if 80 > score >= 60:
        return GeneralRating.Good
    if 60 > score >= 40:
        return GeneralRating.Sufficient
    if score < 40:
        return GeneralRating.Satisfactory


def gre_to_general(score: int) -> GeneralRating:
    if score >= 320:  # max 340
        return GeneralRating.Excellent
    if 300 <= score < 320:
        return GeneralRating.VeryGood
    if 280 <= score < 300:
        return GeneralRating.Good
    if 260 <= score < 280:
        return GeneralRating.Sufficient
    if score < 260:
        return GeneralRating.Satisfactory


def average_fields():
    pass


def __main():
    pass


if __name__ == '__main__':
    __main()
