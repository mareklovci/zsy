#!/usr/bin/env python
# # -*- coding: utf-8 -*-

"""__main__.py: expert system comparison"""

__author__ = "Marek Lovčí"

from app.rule_based import Applicant, School, run_expert_system
from app.ml_system import process_dataframe, process_data, regression, tree_regression


def __main():
    df = process_dataframe()
    x_train, x_test, y_train, y_test = process_data(df)
    clf, score = regression(x_train, x_test, y_train, y_test)
    clf_tree = tree_regression(x_train, x_test, y_train, y_test)

    print('Regression model score: {}\n'.format(score))

    for x, y in zip(x_test, y_test):
        prediction = clf.predict([x])[0]
        prediction_tree = clf_tree.predict([x])[0]
        print('actual: {}\nML model: {}\nTree model: {}'.format(y, round(prediction, 2), round(prediction_tree, 2)))

        # define person
        candidate = Applicant(gre=x[0],
                              toefl=x[1],
                              sop=x[3],
                              lor=x[4],
                              cgpa=x[5],
                              research=x[6])

        school = School(rating=x[2])

        # run expert system
        print('RB model: ', end='')
        run_expert_system(candidate, school)
        print('')


if __name__ == '__main__':
    __main()
