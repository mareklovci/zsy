#!/usr/bin/env python
# # -*- coding: utf-8 -*-

"""__ml_system__.py: expert system implemented using machine-learning"""

__author__ = "Marek Lovčí"

import numpy as np
from sklearn import svm
from sklearn.model_selection import train_test_split
from sklearn import tree

from app.datasets import get_dataframe


def process_dataframe():
    df = get_dataframe()  # read file
    df = np.array(df)  # converting it into an array
    np.random.shuffle(df)  # shuffling
    return df


def process_data(df):
    y = [d[-1] for d in df]  # admission chance
    corpus = [d[0:-1] for d in df]
    # corpus = preprocessing.scale(corpus)  # from sklearn import preprocessing
    x_train, x_test, y_train, y_test = train_test_split(corpus, y, test_size=0.2, random_state=42)
    return x_train, x_test, y_train, y_test


def regression(x_train, x_test, y_train, y_test):
    clf = svm.SVR(kernel='linear')
    clf.fit(x_train, y_train)

    score = clf.score(x_test, y_test)
    return clf, score


def tree_regression(x_train, x_test, y_train, y_test):
    clf = tree.DecisionTreeRegressor()
    clf.fit(x_train, y_train)
    tree.export_graphviz(clf, out_file='tree.dot')
    return clf


def __main():
    df = process_dataframe()
    x_train, x_test, y_train, y_test = process_data(df)
    clf, score = regression(x_train, x_test, y_train, y_test)

    print('SVM regression: {}'.format(score))

    for x, y in zip(x_test, y_test):
        prediction = clf.predict([x])[0]
        rounded_prediction = round(prediction, 2)
        print('Model: {}, actual: {}'.format(rounded_prediction, y))

    print('Tree regression')

    tree_clf = tree_regression(x_train, x_test, y_train, y_test)

    for x, y in zip(x_test, y_test):
        prediction = tree_clf.predict([x])[0]
        rounded_prediction = round(prediction, 2)
        print('Model: {}, actual: {}'.format(rounded_prediction, y))


if __name__ == '__main__':
    __main()
