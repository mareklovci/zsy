#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""datasets.py: Module with functions used for serving data to classifier"""

import os
from os.path import join, dirname
from pathlib import Path

import pandas as pd

data_folder = os.path.normpath(dirname(__file__) + os.sep + os.pardir + '/data/')


def get_dataframe(dataset: str = 'admissions.csv'):
    """Return pandas dataframe for given dataset parameter

    :param dataset: defaults to 'admissions.csv'
    :return: pandas dataframe
    """
    path = Path(join(data_folder, dataset))
    return pd.read_csv(path, ',', error_bad_lines=False, index_col=0)


def __main():
    pass


if __name__ == '__main__':
    __main()
