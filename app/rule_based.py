#!/usr/bin/env python
# # -*- coding: utf-8 -*-

"""rule_based.py: rule-based expert system

GRE = Graduate Record Examinations
TOEFL = Test of English as a Foreign Language
SOP = Statement of Purpose
LOR = Letter of Recommendation Strength
CGPA = Cumulative Grade Point Average
"""

__author__ = "Marek Lovčí"

from pyknow import *

from app.helpers import *


class Applicant(Fact):
    """Info about the applicant"""
    pass


class School(Fact):
    """Info about the school"""
    pass


class Admissions(KnowledgeEngine):
    @Rule(Applicant(research=MATCH.research))
    def has_done_research(self, research):
        if research == 1:
            self.declare(Fact(has_done_research=True))
        else:
            self.declare(Fact(has_done_research=False))

    @Rule(Applicant(cgpa=MATCH.cgpa))
    def cgpa_result(self, cgpa):
        cgpa_score = get_cgpa_score(cgpa)
        general_score = cgpa_to_general(cgpa_score)
        self.declare(Fact(cgpa_result=general_score))

    @Rule(Applicant(lor=MATCH.lor, sop=MATCH.sop))
    def letters_strength(self, lor, sop):
        strength = sum((lor, sop)) / 2
        avg = round(strength)
        self.declare(Fact(letters_strength=GeneralRating(avg)))

    @Rule(Applicant(toefl=MATCH.toefl))
    def toefl_result(self, toefl):
        self.declare(Fact(toefl_result=get_toefl_score(toefl)))

    @Rule(Applicant(gre=MATCH.gre))
    def gre_result(self, gre):
        self.declare(Fact(gre_result=gre_to_general(gre)))

    @Rule(School(rating=MATCH.rating))
    def school_rating(self, rating):
        self.declare(Fact(school_rating=GeneralRating(rating)))

    @Rule(Fact(cgpa_result=MATCH.cgpa_result),
          Fact(letters_strength=MATCH.letters_strength),
          Fact(toefl_result=MATCH.toefl_result),
          Fact(gre_result=MATCH.gre_result),
          Fact(has_done_research=MATCH.has_done_research))
    def overall_rating(self, cgpa_result, letters_strength, toefl_result, gre_result, has_done_research):
        all_results = (cgpa_result, letters_strength, toefl_result, gre_result)
        avg = sum(all_results) / len(all_results)
        if has_done_research and avg < 4.5:
            avg = avg + 0.5
        avg = round(avg)
        self.declare(Fact(overall_rating=GeneralRating(avg)))

    @Rule(Fact(overall_rating=MATCH.overall_rating),
          Fact(school_rating=MATCH.school_rating))
    def applicant_result(self, overall_rating, school_rating):
        rating = school_rating / overall_rating
        rating = rating if rating <= 1.0 else 1.0
        # print('Applicant will be accepted with probability of {} %'.format(rating * 100))
        print(round(rating, 2))


def run_expert_system(candidate: Applicant, school: School):
    # run expert system
    adm = Admissions()
    adm.reset()
    adm.declare(candidate, school)
    adm.run()


def __main():
    # define person
    candidate = Applicant(gre=340,
                          toefl=120,
                          sop=5,
                          lor=5,
                          cgpa=10,
                          research=1)

    school = School(rating=5)

    run_expert_system(candidate, school)


if __name__ == '__main__':
    __main()
